# UTM

The OS is also available to be virtualized on Apple platforms with M1/M2/M3(and its variants) CPUs. Specifically, Parrot can be used through the [open source UTM software](https://mac.getutm.app/).

Once you download and install UTM, you will see this screen:

![utm1](./images/utm/1.png)

The format we provide to users has the **.utm** extension, so it is already compatible with UTM itself, which is why, 
once you download the file from our website, all you have to do is go to the folder where you downloaded Parrot 
and extract the *.utm* file. 

After correctly extracting the *.utm* file, simply drag and drop it inside UTM.

![utm2](./images/utm/2.png)

the OS will be immediately recognized and by clicking on the Play icon you can immediately use Parrot in your system with MacOS.
